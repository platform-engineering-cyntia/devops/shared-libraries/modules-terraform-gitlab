### Modulo Terrafom Gitlab ###
# main.tf #

resource "gitlab_branch" "branch" {

  name    = var.name_branch
  ref     = "master"
  project = var.project
}
