### Modulo Terrafom Gitlab ###
# inputs.tf #

variable "parent_id" {
  description = "Parent Id Gitlab Group"
  type        = string
}

variable "description_group" {
  description = "Descrition Gitlab Group"
  type        = string
}

variable "name_group" {
  description = "Name Gitlab Group"
  type        = string
}

variable "path_group" {
  description = "Path Gitlab Group"
  type        = string
}

variable "share_with_group_lock" {
  description = "value"
  type        = bool
  default     = false
}

variable "prevent_forking_outside_group" {
  description = "value"
  type        = bool
  default     = true
}
