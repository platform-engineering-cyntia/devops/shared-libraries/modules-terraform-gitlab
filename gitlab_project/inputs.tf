### Modulo Terrafom Gitlab ###
# inputs.tf #

variable "project_name" {
  description = "Name Gitlab Project"
  type        = string
}


variable "project_description" {
  description = "Description Gitlab Project"
  type        = string
}


variable "visibility_level" {
  description = "Visibility Level Gitlab Project"
  type        = string
}

variable "namespace_id" {
  description = "Namespace Id Gitlab Project"
  type        = string
}
