### Modulo Terrafom Gitlab ###
# main.tf #

resource "gitlab_project" "project" {

  name             = var.project_name
  description      = var.project_description
  visibility_level = var.visibility_level
  namespace_id     = var.namespace_id
}
