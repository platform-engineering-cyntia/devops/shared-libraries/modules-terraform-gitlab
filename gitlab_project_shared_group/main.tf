### Modulo Terrafom Gitlab ###
# main.tf #


resource "gitlab_project_share_group" "group_member" {

  project_id   = var.project_id
  group_id     = var.group_id
  group_access = var.group_access

}
