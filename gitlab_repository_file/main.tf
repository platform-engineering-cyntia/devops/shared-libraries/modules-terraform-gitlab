### Modulo Terrafom Gitlab ###
# main.tf #

resource "gitlab_repository_file" "file" {

  project        = var.repo_id
  file_path      = var.repo_path
  branch         = var.repo_branch
  content        = var.repo_content
  author_email   = var.author_email
  author_name    = var.author_name
  commit_message = var.commit_message

}
